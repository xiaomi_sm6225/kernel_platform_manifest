Clo Kernel platform Manifest Linux 5.15
=========================================

This manifest used for build bengal,khaje target based kernel source,devicetree and external modules with minimal enviroment.Without syncing full rom  source.

manifest tag : KERNEL.PLATFORM.2.0.r1-09100-kernel.0

## How to sync source?
1. Intitalize your local repository using this manifest:
```
repo init -u https://gitlab.com/xiaomi_sm6225/kernel_platform_manifest.git -b main
```
2. Then to sync up:
```
repo sync
```
3. clone your Kernel source on kernel platform source root dir and rename it msm-kernel:

4. After that clone your external kernel modules on kernel platform source root dir. Example
```
vendor/qcom/opensource/camera-kernel
```
5. then create your own build script (ex: build.sh) and start build.